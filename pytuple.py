ls=['red','green','blue']
# ls[1]='yellow'
# print(ls)

# t=tuple(ls)
# t[1]='green'            # it produce error:bcz tuple does nt support item assignment so it is used as secure method to define a list
# print(t)

# c=ls.count('red')
# print(c)

#Creating a tuple
tp=(10,20,50,34,344,78,70)
print(tp[0])
print(tp[-1])
print(tp[1:])
print(tp[:-1])
print(tp[::2])
print(tp[::3])