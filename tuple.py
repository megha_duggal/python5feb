#Type Casting
ls=['red','green','blue']
st=int('103')
n=10
#Convert to a string
convert_to_string=str(n)
#print(type(n))
#print(type(st))
#print(type(convert_to_string))

#convert to boolean
b=bool(n)
print(type(b))

#Convert to float
# f=float(n)
# print(type(f))
# print(f)

#Convert to list
#l=list('ljshhjkjk')     #numbers can not be converted to list bcz we can't index numbers;bt possible in string
#print(l)
ab='String'
#print(list(ab))

#Convert to tuple
t=tuple(ab)
print(t)